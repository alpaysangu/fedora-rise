## Description

The fedora minimal installation script that I created for my computer.

## Usage

Download the everything iso from [here](https://alt.fedoraproject.org/)

In the software selection menu, select the 'minimal install' option

### Local Files

Create server

```bash
make
```

Execute setup bash script on the client

```bash
curl --silent [created-server-address]/setup.sh | sudo bash
```

Download setup make file from the server

```bash
curl --silent [created-server-address]/setup.mk >> makefile
```

Execute setup make file on the client

```bash
make
```

### Remote Files

Execute setup bash script on the client

```bash
curl --silent https://gitlab.com/api/v4/projects/36160759/repository/files/source/setup.sh/raw | sudo bash
```

Download setup make file from the repository

```bash
curl --silent https://gitlab.com/api/v4/projects/36160759/repository/files/source/setup.mk/raw >> makefile
```

Execute setup make file on the client

```bash
make
```

## Useful Links

[Minimal install example one](https://linuxx.info/a-minimal-installation-of-fedora/)

[Minimal install example two](https://able.bio/KY64/minimal-installation-fedora-linux--73410e6d)
